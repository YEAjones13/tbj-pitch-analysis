import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sklearn as sk
import seaborn as sb

data = pd.read_csv("tbj-pitch-analysis/pitchdata.csv")

data.head(10)

###CHECKING FOR NULL VALUES###
data.info()

### FILLING NULL VALUES WITH MEDIAN ###
mean1 = data.PlateHeight.mean()
data['PlateHeight'].fillna(value=mean1, inplace=True)
mean2 = data.PlateSide.mean()
data['PlateSide'].fillna(value=mean2, inplace=True)
data.info()

### CHECKING THE UNIQUE VALUES FOR HANDEDNESS ###
data.PitcherHand.unique()
data.BatSide.unique()

### CHECKING TO MAKE SURE THE COUNTS MAKE SENSE ###
data.Strikes.unique()
data.Balls.unique()

data.GameID.value_counts()
### 404 Different Games ###

### NUMBER OF CALLED STRIKES ###
data.CalledStrike.value_counts()

### CHECKING IF THERE'S OTHER NUMBERS OTHER THAN 0 AND 1 ###
data.CalledStrike.unique()

###DROPPING GAMEID###
data1 = data.drop('GameID', axis =1)

### REPLACED 'R' AND 'L' VALUES TO '1' AND '2' RESPECTIVELY ###
data1.loc[data1['PitcherHand'] == 'R', 'PitcherHand'] = '1'
data1.loc[data1['PitcherHand'] == 'L', 'PitcherHand'] = '2'
data1.loc[data1['BatSide'] == 'R', 'BatSide'] = '1'
data1.loc[data1['BatSide'] == 'L', 'BatSide'] = '2'

###CREATING A COLUMN SHOWING IF ITS A L-L OR R-R MATCHUP###
matchups = []

for index, row in data1.iterrows():
    if (row['PitcherHand'] == row['BatSide']):
        matchups.append(1)
    else:
        matchups.append(0)

data1['Matchups'] = matchups


### CATEGORIZING PITCHNUMBER INTO 1,2 OR 3 (DIFFERENT TIMES OF THE GAME) IE: 1 = 1ST 3INN, 2 = 4-6INN, 3 = 7-9INN ###
data1['Time of Game'] = data1['PitchNumber'].apply(
    lambda x: 1 if 1 <= x <= 124 
    else 2 if 125 <= x <= 250 else 3)

data1 = data1.drop('PitchNumber', axis = 1)

data1.corr()

dataplot = sb.heatmap(data1.corr(), cmap='Reds', annot=True, cbar=True)
print(dataplot)

